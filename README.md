# Kubernetes MASTER and NODEs (1 and 2) on single host
## HOST
For this tutorial i used a fresh minimal installed CentOS 7 dedicated Server from HETZNER

* CUP Intel(R) Core(TM) i7-2600
* MEM 16 GB
* 2 x Disk 3 TB (software raid)

## Workstation 
My WINDOWS 10 machine 

# Use virtualbox

https://www.itzgeek.com/how-tos/linux/centos-how-tos/install-virtualbox-4-3-on-centos-7-rhel-7.html

you will create following setup

## CPU Cores for virtual machines
* 2 kubernetes master
* 2 kubernetes node 1
* 2 kubernetes node 2

## Memory
* 4 GB kubernetes master
* 2 GB kubernetes node 1
* 2 GB kubernetes node 2

## Disk
* 500 GB kubernetes master
* 500 GB kubernetes node 1
* 500 GB kubernetes node 2

login into your remote server

```
yum update
shutdown -r now
yum install -y kernel-devel kernel-headers gcc make perl wget net-tools
```

Open firewall ports

```
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --zone=public --add-port=443/tcp --permanent
firewall-cmd --zone=public --add-port=220/tcp --permanent
firewall-cmd --zone=public --add-port=221/tcp --permanent
firewall-cmd --zone=public --add-port=222/tcp --permanent
firewall-cmd --zone=public --add-port=3000/tcp --permanent
firewall-cmd --zone=public --add-port=8001/tcp --permanent
firewall-cmd --zone=public --add-port=8080/tcp --permanent
firewall-cmd --zone=public --add-port=6443/tcp --permanent
firewall-cmd --zone=public --add-port=33000/tcp --permanent
firewall-cmd --reload
```

Create the '**/etc/yum.repos.d/virtualbox.repo**' file with the following repository information.

```
touch /etc/yum.repos.d/virtualbox.repo

echo "[virtualbox]" >> /etc/yum.repos.d/virtualbox.repo
echo "name=Oracle Linux / RHEL / CentOS-$releasever / $basearch - VirtualBox" >> /etc/yum.repos.d/virtualbox.repo
echo "baseurl=http://download.virtualbox.org/virtualbox/rpm/el/$releasever/$basearch" >> /etc/yum.repos.d/virtualbox.repo
echo "enabled=1" >> /etc/yum.repos.d/virtualbox.repo
echo "gpgcheck=1" >> /etc/yum.repos.d/virtualbox.repo
echo "repo_gpgcheck=1" >> /etc/yum.repos.d/virtualbox.repo
echo "gpgkey=https://www.virtualbox.org/download/oracle_vbox.asc" >> /etc/yum.repos.d/virtualbox.repo
```

```
yum install -y VirtualBox-6.0
```

```
mkdir -p /var/virtualbox/images
mkdir -p /var/virtualbox/machines/kubernetes
VBoxManage setproperty machinefolder /var/virtualbox/machines
```

## Download image and extension pack

```
wget -P /var/virtualbox/images/ http://centos.mirroraustria.at/7.6.1810/isos/x86_64/CentOS-7-x86_64-Minimal-1810.iso
wget -P /var/virtualbox/images/ http://download.virtualbox.org/virtualbox/6.0.0/Oracle_VM_VirtualBox_Extension_Pack-6.0.0.vbox-extpack
wget -P /var/virtualbox/images/ https://download.virtualbox.org/virtualbox/6.0.0/VBoxGuestAdditions_6.0.0.iso
VBoxManage extpack install /var/virtualbox/images/Oracle_VM_VirtualBox_Extension_Pack-6.0.0.vbox-extpack
```

# Kubernetes MASTER

```
VBoxManage createvm --name "kubernetes-master" --ostype RedHat_64 --register --groups /kubernetes

VBoxManage modifyvm "kubernetes-master" --memory 4096 --cpus 2 --vrdeport 33000 --nic1 nat --natpf1 "ssh,tcp,,220,,22" --nic2 intnet --intnet2 kubernetes

VBoxManage createhd --filename /var/virtualbox/machines/kubernetes/kubernetes-master/kubernetes-master.vdi --size 500000 --format VDI
VBoxManage storagectl "kubernetes-master" --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach "kubernetes-master" --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium /var/virtualbox/machines/kubernetes/kubernetes-master/kubernetes-master.vdi
VBoxManage storagectl "kubernetes-master" --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach "kubernetes-master" --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium /var/virtualbox/images/CentOS-7-x86_64-Minimal-1810.iso

VBoxHeadless --startvm "kubernetes-master" &

VBoxManage controlvm "kubernetes-master" vrde on
```

get IP-Address with **ipconfig**

```
ifconfig -a
```

Connect to Virtual Machine with Windows Remotedesktop with your host IP-Address or FQHN **xxx.xxx.xxx.xxx:33000**


* follow the installation instructions
* disk settings create one big / partition and one /boot (1GB) partition
* after installation is finished a restart is required

![alt text](partitions.png)

login into new VM as root and adjust network settings

use '**nmtui**' for network configuration

in my case INTERFACEs are **ifcfg-enp0s3** and **ifcfg-enp0s8**

**enp0s3** activate automatic connection

**enp0s8** activate automatic connection
* Network Address 192.168.0.10


```
ip addr list
nmtui
systemctl restart network
exit
```

## create ssh keys

* easy login from HOST
* easy login from YOUR WINDOWS 


### login from HOST

on your HOST machine

* if you not already have, generated a key pair

```
ssh-keygen
```

#### copy keys to remote machines

```
ssh-copy-id -i $HOME/.ssh/id_rsa.pub -p 220 root@localhost
```

### install putty and openssh on windows

on your windows machine

* if you have not already installed openssh => https://www.ssh.com/ssh/openssh/
* you will need putty if you not have it already installed => https://www.putty.org/

```
ssh-keygen

cat "%HOMEPATH%/.ssh/id_rsa.pub" | ssh -p 220 root@kube.appcomplete.at "cat >> ~/.ssh/authorized_keys"

puttygen "%HOMEPATH%/.ssh/id_rsa" -o "%HOMEPATH%/.ssh/id_rsa.pub.pkk"
```

use this tutorial for puttty setup => https://www.howtoforge.de/anleitung/key-basierte-ssh-logins-mit-putty/3/

### basic settings

* install and update some packages
* disable SWAP partition (only if a SWAP partition was created!)
* disable firwall
* disable SElinux
* network bridge settings
* enable kernel br_netfilter module on boot
* set hostname
* setup dns resolution for all hosts
* add repositories
* install docker and kubernetes
* create kubernetes user (k8s-user)

```
ssh -p 220 localhost
yum -y install wget lsof net-tools traceroute vim yum-utils device-mapper-persistent-data lvm2 git dkms gcc make kernel-devel bzip2 binutils patch libgomp glibc-headers glibc-devel kernel-headers
yum -y update

vim /etc/fstab
systemctl disable firewalld && systemctl stop firewalld

echo "SELINUX=disabled" > /etc/selinux/config
echo "SELINUXTYPE=targeted" >> /etc/selinux/config

touch /etc/sysctl.d/k8s.conf
echo "net.bridge.bridge-nf-call-ip6tables = 1" >> /etc/sysctl.d/k8s.conf
echo "net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.d/k8s.conf

touch /etc/modules-load.d/br_netfilter.conf
echo "br_netfilter" >> /etc/modules-load.d/br_netfilter.conf

echo "192.168.0.10   master  kube-master  kube-master.appcomplete.at  kube.master.appcomplete.at" >> /etc/hosts
echo "192.168.0.11   node1   kube-node1   kube-node1.appcomplete.at   kube.node1.appcomplete.at" >> /etc/hosts
echo "192.168.0.12   node2   kube-node2   kube-node2.appcomplete.at   kube.node2.appcomplete.at" >> /etc/hosts

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum-config-manager --enable docker-ce-edge

touch /etc/yum.repos.d/kubernetes.repo
echo "[kubernetes]" >> /etc/yum.repos.d/kubernetes.repo
echo "name=Kubernetes" >> /etc/yum.repos.d/kubernetes.repo
echo "baseurl=http://yum.kubernetes.io/repos/kubernetes-el7-x86_64" >> /etc/yum.repos.d/kubernetes.repo
echo "enabled=1" >> /etc/yum.repos.d/kubernetes.repo
echo "gpgcheck=1" >> /etc/yum.repos.d/kubernetes.repo
echo "repo_gpgcheck=1" >> /etc/yum.repos.d/kubernetes.repo
echo "gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg" >> /etc/yum.repos.d/kubernetes.repo
echo "       https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg" >> /etc/yum.repos.d/kubernetes.repo

hostnamectl set-hostname "kube-master.appcomplete.at"

yum -y install docker-ce-18.06.0.ce-3.el7 kubectl kubelet kubeadm kubernetes-cni --disableexcludes=kubernetes

echo "exclude=docker-ce*" >> /etc/yum.conf

systemctl enable docker
systemctl start docker
systemctl status docker

systemctl enable kubelet
systemctl start kubelet
systemctl status kubelet

adduser k8s-user
usermod -aG wheel k8s-user
passwd k8s-user
echo "k8s-user ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/k8s-user

shutdown -h now
```

create a snapshot and a clone of a basic centos 7 installation (you dont know how often you will need it :))

```
VBoxManage snapshot "kubernetes-master" take basic
VBoxManage clonevm "kubernetes-master" --name "centos7_kubernetes_basic" --register --groups /
```

**restart from here** if something went wrong you can restart from here 

```
VBoxManage snapshot "kubernetes-master" restore basic
VBoxManage unregistervm "kubernetes-node1" --delete
VBoxManage unregistervm "kubernetes-node2" --delete
VBoxHeadless --startvm "kubernetes-master" &
```

# Kubernetes NODEs

* clone machine for nodes 

```
VBoxManage modifyvm "kubernetes-master" --natpf1 delete ssh

VBoxManage clonevm "kubernetes-master" --name "kubernetes-node1" --register --groups /kubernetes

VBoxManage clonevm "kubernetes-master" --name "kubernetes-node2" --register --groups /kubernetes

VBoxManage modifyvm "kubernetes-node1" --memory 2048 --vrdeport 33001
VBoxManage modifyvm "kubernetes-node1" --natpf1 "ssh,tcp,,221,,22"
VBoxManage modifyvm "kubernetes-node2" --memory 2048 --vrdeport 33002
VBoxManage modifyvm "kubernetes-node2" --natpf1 "ssh,tcp,,222,,22"

VBoxHeadless --startvm "kubernetes-node1" &
VBoxHeadless --startvm "kubernetes-node2" &

VBoxManage controlvm "kubernetes-node1" vrde off
VBoxManage controlvm "kubernetes-node2" vrde off
```

# proceed with MASTER

* add all ports to nat master
* prepare master for GUI installation

```
VBoxManage modifyvm "kubernetes-master" --natpf1 "ssh,tcp,,220,,22"
VBoxManage modifyvm "kubernetes-master" --natpf1 "https,tcp,,443,,443"
VBoxManage modifyvm "kubernetes-master" --natpf1 "https-dashboard,tcp,,6443,,6443"
VBoxManage modifyvm "kubernetes-master" --natpf1 "http,tcp,,80,,80"
VBoxManage modifyvm "kubernetes-master" --natpf1 "http-kubernator,tcp,,3000,,3000"
VBoxManage modifyvm "kubernetes-master" --natpf1 "http-dashboard,tcp,,8001,,8001"
VBoxManage modifyvm "kubernetes-master" --natpf1 "http-remote,tcp,,8080,,8080"

VBoxManage storageattach "kubernetes-master" --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium /var/virtualbox/images/VBoxGuestAdditions_6.0.0.iso
VBoxManage modifyvm "kubernetes-master" --vram 256

VBoxHeadless --startvm "kubernetes-master" &
```

## set hostnames and ip addresses

* set hostname for each node
* set IP-Address of '**enp0s8**' to (192.168.0.11 and 12) for each node

```
ssh -p 221 localhost
hostnamectl set-hostname "kube-node1.appcomplete.at"
nmtui
systemctl restart network
exit

ssh -p 222 localhost
hostnamectl set-hostname "kube-node2.appcomplete.at"
nmtui
systemctl restart network
exit
```

## install gui

* install graphical user interface and firefox quantum

```
ssh -p 220 localhost
yum -y groupinstall "Server with GUI"
yum -y update
```

### install firefox quantum

select OS and language for latest version

* https://download-installer.cdn.mozilla.net/pub/firefox/releases/latest/README.txt

```
yum -y remove firefox

cd /opt
wget -O firefox-latest.tar.bz2 "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=de"
tar xfj firefox-latest.tar.bz2

touch "/usr/share/applications/firefox-quantum.desktop"
echo "[Desktop Entry]" > "/usr/share/applications/firefox-quantum.desktop"
echo "Version=latest" >> "/usr/share/applications/firefox-quantum.desktop"
echo "Name=Firefox" >> "/usr/share/applications/firefox-quantum.desktop"
echo "Exec=/opt/firefox/firefox" >> "/usr/share/applications/firefox-quantum.desktop"
echo "Terminal=false" >> "/usr/share/applications/firefox-quantum.desktop"
echo "Type=Application" >> "/usr/share/applications/firefox-quantum.desktop"
echo "StartupNotify=true" >> "/usr/share/applications/firefox-quantum.desktop"
echo "Categories=Network;WebBrowser;" >> "/usr/share/applications/firefox-quantum.desktop"
echo "X-Desktop-File-Install-Version=0.15" >> "/usr/share/applications/firefox-quantum.desktop"
echo "Icon=/opt/firefox/browser/chrome/icons/default/default128.png" >> "/usr/share/applications/firefox-quantum.desktop"
```

### install virtual box guest edition

```
mkdir -p /media/cdrom
ls /dev -l | grep dvd
ls /dev -l | grep cd
mount -r -t iso9660 /dev/sr0 /media/cdrom
cd /media/cdrom/
sh VBoxLinuxAdditions.run
ln -sf /lib/systemd/system/runlevel5.target /etc/systemd/system/default.target
shutdown -r now
```

## create snapshots

```
ssh -p 221 localhost
shutdown -h now
ssh -p 222 localhost
shutdown -h now
ssh -p 220 localhost
shutdown -h now

VBoxManage snapshot "kubernetes-master" delete everything
VBoxManage snapshot "kubernetes-node1" delete everything
VBoxManage snapshot "kubernetes-node2" delete everything

VBoxManage snapshot "kubernetes-master" take everything
VBoxManage snapshot "kubernetes-node1" take everything
VBoxManage snapshot "kubernetes-node2" take everything

VBoxHeadless --startvm "kubernetes-master" &
VBoxHeadless --startvm "kubernetes-node1" &
VBoxHeadless --startvm "kubernetes-node2" &
```

**restore** snapshot (if something went wrong!) run follwoing commands

```
VBoxManage controlvm "kubernetes-master" poweroff
VBoxManage controlvm "kubernetes-node1" poweroff
VBoxManage controlvm "kubernetes-node2" poweroff

VBoxManage snapshot "kubernetes-master" restore everything
VBoxManage snapshot "kubernetes-node1" restore everything
VBoxManage snapshot "kubernetes-node2" restore everything

VBoxHeadless --startvm "kubernetes-master" &
VBoxHeadless --startvm "kubernetes-node1" &
VBoxHeadless --startvm "kubernetes-node2" &
```

## configure master
kubeadm init --apiserver-advertise-address=192.168.0.10
 --apiserver-advertise-address=10.0.2.15
 --pod-network-cidr=192.168.0.0/26
 --pod-network-cidr=192.168.0.1/24
 --eviction-soft=memory.available<800Mi,nodefs.available<3Gi,imagefs.available<6Gi
 --eviction-soft-grace-period=memory.available=10m,nodefs.available=10m,imagefs.available=10m
 --eviction-max-pod-grace-period=-1
 --eviction-hard=memory.available<300Mi,nodefs.available<1Gi
 --eviction-pressure-transition-period=10m
 --eviction-soft="memory.available<800Mi,nodefs.available<3Gi,imagefs.available<6Gi" --eviction-soft-grace-period="memory.available=10m,nodefs.available=10m,imagefs.available=10m" --eviction-max-pod-grace-period="-1" --eviction-hard="memory.available<300Mi,nodefs.available<1Gi" --eviction-pressure-transition-period="10m"

```
ssh -p 220 localhost

sysctl --all | grep "net.bridge.bridge-nf"

kubeadm init --apiserver-advertise-address=192.168.0.10 --pod-network-cidr=192.168.0.0/16

su k8s-user

sudo mkdir -p $HOME/.kube
sudo rm --force $HOME/.kube/config
sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
export KUBECONFIG=$HOME/.kube/config

kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

exit

curl -L git.io/weave -o /usr/local/bin/weave
chmod a+x /usr/local/bin/weave
```

## install and configure dashboard

https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/
https://github.com/kubernetes/dashboard/wiki/Creating-sample-user
http://kube.appcomplete.at:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

```
su k8s-user

kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
nohup kubectl proxy --address="0.0.0.0" -p 8001 --accept-hosts='^*$' &
```


## install and configure kubernator

https://github.com/smpio/kubernator

```
su k8s-user

kubectl create ns kubernator
kubectl -n kubernator run --image=smpio/kubernator --port=80 kubernator
kubectl -n kubernator expose deploy kubernator
nohup kubectl proxy --address="0.0.0.0" -p 3000 --accept-hosts='^*$' &
```

## configure nodes

```
ssh -p 221 localhost
=>kubeadm join ...
su k8s-user
mkdir -p $HOME/.kube
sudo cp /etc/kubernetes/kubelet.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
export KUBECONFIG=$HOME/.kube/config
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
exit


ssh -p 222 localhost
=>kubeadm join ...
su k8s-user
mkdir -p $HOME/.kube
sudo cp /etc/kubernetes/kubelet.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
export KUBECONFIG=$HOME/.kube/config
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
exit
```

# Cheat Sheet

## kubernetes

https://kubernetes.io/docs/reference/kubectl/cheatsheet/

```
kubeadm reset

su k8s-user
 
kubectl cluster-info
kubectl get pods --all-namespaces

kubectl get nodes
kubectl describe nodes

kubectl delete node kube-node1.appcomplete.at
kubectl delete node kube-node2.appcomplete.at

sudo kubeadm token create --print-join-command

sudo /usr/local/bin/weave status

sudo systemctl status kubelet
```

## GUI

```
VBoxManage controlvm "kubernetes-master" vrde off
VBoxManage controlvm "kubernetes-master" vrde on
```

open Remotedesktop connection with IP or FQHN '**xxx.xxx.xxx.xxx:33000**'

* login

## Start all machines

```
VBoxHeadless --startvm "kubernetes-master" &
VBoxHeadless --startvm "kubernetes-node1" &
VBoxHeadless --startvm "kubernetes-node2" &
```

## Turn off external access to your virtual machine

```
VBoxManage list vms -l
VBoxManage list runningvms

VBoxManage showvminfo "kubernetes-master"

VBoxManage controlvm "kubernetes-master" poweroff
VBoxManage controlvm "kubernetes-node1" poweroff
VBoxManage controlvm "kubernetes-node2" poweroff
```

## remove everything

```
VBoxManage unregistervm "kubernetes-master" --delete
VBoxManage unregistervm "kubernetes-node1" --delete
VBoxManage unregistervm "kubernetes-node2" --delete
```
